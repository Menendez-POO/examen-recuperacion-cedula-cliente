<script language="javascript">
        document.querySelector('#boton').addEventListener('click', datoscliente());

        function datoscliente() {
            const xhttp = new XMLHttpRequest();
            xhttp.open('GET', 'actualizar.json', true);
            xhttp.send();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    // console.log(this.responseText);
                    let datos = JSON.parse(this.responseText);
                    console.log(datos);
                    let res = document.querySelector('#respuesta');
                    res.innerHTML = '';
                    //ciclo for para acceder a los datos dentro del array 
                    for (let item of datos) {
                        res.innerHTML += `<tr>
                 <td>${item.correo}</td>`

                    }
                }
            }
        };
        // validaciones
        var validar_correo = /^\w+@\w+\.+[a-z]{2,3}$/;
        var validarRegistro = document.getElementById("formulario-registro");
        validarRegistro.onsubmit = function() {
            let correo = document.getElementById('correo').value;
            if (!validar_correo.test(correo)) {
                alert("Lo sentimos, el correo debe de ser tipo email");
                return false;
            } else {
                alert("Registro completado");
                validarRegistro.reset();
            }
        };
    </script>